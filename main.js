const canvas = document.getElementById('myCanvas');
let ctx = canvas.getContext('2d');
const data = [];

const drawBtn = document.getElementById('drawBtn');
const height = document.getElementById('height');
const width = document.getElementById('width');
const A = document.getElementById('A');
const T = document.getElementById('T');
const F = document.getElementById('F');
const dataList = document.getElementById('data')

const calcY = (x, A, T, F) => {
    return A*Math.sin(x/T + F);
};

const draw = (height, width, A, T, F) => {
    for (let x=0; x<width ; x++) {
        ctx.fillStyle = 'black';
        ctx.fillRect(x, calcY(x, A, T, F)+height/2, 1, 1);
    }
};

const setData = (A, T, F) => {
    for (let x=0; x<T*10; x++) {
        data.push({
            x,
            y: calcY(x, A, T, F),
            A, 
            T,
            F
        })
    }
};

drawBtn.addEventListener('click', () => {
    canvas.height = height.value ? height.value : canvas.height;
    canvas.width = width.value ? width.value : canvas.width;
    draw(canvas.height, canvas.width, A.value, T.value, F.value);
    setData(A.value, T.value, F.value);
    dataList.innerHTML = data?.map(el => {
        return `<li>x: ${el.x}, y: ${el.y}, T: ${el.T}, A: ${el.A}, F: ${el.F}</li>`
    }).join('');
});

